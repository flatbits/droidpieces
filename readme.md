# DroidPieces

DroidPieces is a way to create and manage a private maven repository for Android Modules.

## Unmaintained project
This project was a simple idea when Android Studio was still new. Google made it a lot easier to create Android modules now and this project as not added value.
Code will stay up as reference, but there won't be any more support.

### Documentation and tools currently only works in Mac OSX. Linux and Windows support are on their way!

## Repository Installation

There are some prerequisites in order to use this repository, either for grabbing a module or creating one.

### Automated Installation

Luckily for you I've created a small installation script that will install everything you'll need to get you going or will guide you for the things it can't do on itself.
To use that script simply open up a Terminal and navigate to this repository root directory and type
```bash
./tools/install.sh
```

### Manual Installation
#### This manual installation documentation is outdated, please follow the install.sh comments to install. The documentation will be updated soon.

First, if the installation script did not work for you, please open up an issue in BitBucket.

The following steps are basically the same steps followed by the installation script :

1. **Download the latest version of [Maven](http://maven.apache.org/download.cgi)** and extract it to where you want to install it. _The installation script installs it into **~/dev/maven**_

1. **Add it to your `PATH` environment variable.** You can simply add the following line into your **~/.bash_profile** file : `export PATH=/path/to/your/maven/bin:${PATH}`. _You will need to restart any active Terminal or type in the same line for it to work._

1. **Make sure your `JAVA_HOME` environment variable points to a 1.7+ jdk.** Add the following line in your **~/.bash_profile** file : `export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home`. _Change the jdk version to the one you have installed on your system._

1. **Test that your maven installation is working** by typing `mvn --version` into a Terminal. If you have no java exceptions, everything is fine.

1. **Configure the local maven repository to point to this repository.** Open up your maven's settings.xml file (maven_home/conf/settings.xml) and add the following line to it : `<localRepository>/path/to/this/repo/release</localRepository>`. _Make sure you add the /release at the end of the path, it's this directory that maven will parse when looking for modules_

## Using Modules

You will need to tell your android project to look into your local Maven Repository by adding the following lines in your app's **build.gadle** file :

```
android{
...
    repositories{
        mavenLocal()
    }
...
}
```

You can then use modules the same way you usually do it with Android Studio : simply add a _compile line_ in your **gradle.build** file and sync!

Example : `compile 'com.flatbits.ui:infinitepager:1.1'`

## Creating and Updating Modules

### Module Creation

#### Piece Creator

This script will do the hard work for you and generate the frame of your project to get you going faster.

**Usage :** `./tools/piece_creator.sh ModuleName [com.example.packagename]`

You can then open up the generated folder directly in Android Studio!

### Piece Packaging and Deployment

Packaging and deploying a module is actually really easy : configure you're package information, launch a gradle script and you're done!

1. Open up your app's **build.gradle** file and localise the _installArchives_ section.

1. Make sure that the `pom.groupId`, `pom.artifactId` and `pom.version` values are to your liking.

1. On the right section of Android Studio. **Click on the _Gradle_ tab**.

1. Expand all the tasks and look for the one named **_installArchives_** and execute it. _Should be under **other** subfolder_

1. Make sure everything went well by opening up Finder and navigating to this repository's release folder. You can then go down the directories (that represents your _groupId_ and _artifactId_) and look for your newborn module.

1. Don't forget to update your module's **readme.md** file that should be at the root of your module's folder.

1. Commit your module to your repository and tell everyone so they can all enjoy how awesome it is.

## Contact

If you have any question or trouble using this repository please open up an issue in BitBucket and I'll look into it!
