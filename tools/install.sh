#!/bin/bash
# Installation script for Flatbits's Droid Pieces

command -v mvn >/dev/null 2>&1
if [ $? != 0 ]; then
    # Maven is not installed install it!
    echo "Maven not detected on this computer, fetching and install Maven 3.3.1 now..."
    # Create maven directory and move to it
    mkdir -p ~/dev/maven
    cd ~/dev/maven
    # Download and extract
    curl -O http://apache.mirror.vexxhost.com/maven/maven-3/3.3.1/binaries/apache-maven-3.3.1-bin.tar.gz
    tar -xzvf apache-maven-3.3.1-bin.tar.gz --strip-components=1
    rm apache-maven-3.3.1-bin.tar.gz
    # Add to PATH
    mavenHome=$(pwd -P)
    mavenHome="${mavenHome}/bin"
    export PATH=${mavenHome}:${PATH}
    # Making it permanent
    echo "export PATH=${mavenHome}:\${PATH}" >> ~/.bash_profile
fi

mvn --version >/dev/null 2>&1
if [ $? != 0 ]; then
    echo "JAVA_HOME not set correctly, trying to detect latest jdk installed..."
    # By default, jdks are installed in the folder below
    jdkDir="/Library/Java/JavaVirtualMachines/"
    if [ ! -d "$jdkDir" ]; then
        echo ""
        echo "No jdk detected."
        echo "Please make sure you have a jdk installed in '${jdkDir}' and try again."
        exit 1
    fi
    # Get the latest one
    latestJdk=$(ls -t $jdkDir | head -1)
    javaHome="${jdkDir}${latestJdk}/Contents/Home"
    if [ ! -d "$javaHome" ]; then
        echo ""
        echo "No jdk detected."
        echo "Please make sure you have a jdk installed in '${javaHome}' and try again."
        exit 1
    fi

    # Set it as JAVA_HOME
    export JAVA_HOME=${javaHome}
    # Make it permanent
    echo "export JAVA_HOME=${javaHome}" >> ~/.bash_profile
fi

# Maven command is now correctly installed, now we need to configure it
echo "Configuring Maven..."

# We want to configure the local maven instance to use our repository as local repo
scriptDir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
repoDir="${scriptDir%/*}/release"
echo $repoDir
# We basically want to insert our repo dir in the maven's settings.xml
# We'll use some kung-fu style string substitution to do so
mavenDir=$(which mvn)
mavenDir=$(dirname "${mavenDir}")
mavenSettingsFile="${mavenDir}/../conf/settings.xml"
sed -i -e "s|<localRepository>/path/to/local/repo</localRepository>|--><localRepository>${repoDir}</localRepository><!--|g" $mavenSettingsFile


# One last thing we need to tell Android Studio to use our instance of maven
# We do this by setting M2_HOME globally for the current user
# View : http://stackoverflow.com/a/26586170

mkdir -p ~/Library/LaunchAgents
cat > ~/Library/LaunchAgents/environment.plist <<- EOM
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>Label</key>
  <string>my.startup</string>
  <key>ProgramArguments</key>
  <array>
    <string>sh</string>
    <string>-c</string>
    <string>
    launchctl setenv M2_HOME ${mavenDir%/*}
    </string>

  </array>
  <key>RunAtLoad</key>
  <true/>
</dict>
</plist>
EOM
launchctl unload ~/Library/LaunchAgents/environment.plist
launchctl load ~/Library/LaunchAgents/environment.plist


# We're done!
echo "Installation successful"
echo ""
echo "*** IMPORTANT ***"
echo "Please restart your bash window and Android Studio for it to load the new path variables"
echo "If it still does not work, please try to restart your computer"