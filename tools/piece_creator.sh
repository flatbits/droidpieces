#!/bin/bash
# Creates a new android module project

# Give some space to delimit the prints of this command (easier to spot where it begin)
# Also prints header with credits!
echo ""
echo "**********         Piece Creator          **********"
echo "**********    A tool brough to you by     **********"
echo "**********   Maxime Bélanger - FlatBits   **********"
echo ""

scriptDir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
repoDir="$(dirname ${scriptDir})"

# Checking if required arguments are present
if [ -z "$1" ]; then
    echo "Please specify a name for the module"
    echo "usage: ./piece_creator.sh module_name [package_namespace]"
    exit 1
fi

projectName=$1

projectRoot="${repoDir}/${projectName}"

# copy the template project to this repo root
cp -R "${scriptDir}/template" $projectRoot

#start renaming things
lowerProjectName=$(echo $projectName | tr '[:upper:]' '[:lower:]')

mv "${projectRoot}/ProjectName.iml" "${projectRoot}/${projectName}.iml"
echo $projectName > "${projectRoot}/.idea/.name"

sed -i '' -e "s|ProjectName|${projectName}|g" "${projectRoot}/.idea/modules.xml"
sed -i '' -e "s|ProjectName|${projectName}|g" "${projectRoot}/.idea/workspace.xml"
sed -i '' -e "s|ProjectName|${projectName}|g" "${projectRoot}/app/app.iml"
sed -i '' -e "s|ProjectName|${projectName}|g" "${projectRoot}/readme.md"

sed -i '' -e "s|projectname|${lowerProjectName}|g" "${projectRoot}/app/build.gradle"
sed -i '' -e "s|projectname|${lowerProjectName}|g" "${projectRoot}/app/src/main/AndroidManifest.xml"
sed -i '' -e "s|projectname|${lowerProjectName}|g" "${projectRoot}/readme.md"

# If a second parameter was provided, replace the default package name by the custom one
packageName="com.example"
if [ ! -z "$2" ]; then
    packageName=$2

    sed -i '' -e "s|com.example|${packageName}|g" "${projectRoot}/app/build.gradle"
    sed -i '' -e "s|com.example|${packageName}|g" "${projectRoot}/app/src/main/AndroidManifest.xml"
    sed -i '' -e "s|com.example|${packageName}|g" "${projectRoot}/readme.md"
fi
# Create the folder structure for the java file (based on package name)
packageFolder=$(echo ${packageName} | tr . /)
mkdir -p "${projectRoot}/app/src/main/java/${packageFolder}"

echo "Project ${projectName} created successfully"
exit 0
# Legacy method below using android tools instead...

# We first need to get a reference to the android sdk folder to access the tools we need
# if [ -z $ANDROID_HOME ]; then
#     echo "ANDROID_HOME not found"
#     echo "Please configure ANDROID_HOME as an environment variable pointing to your android sdk path and try again"
#     echo "******"
#     echo "Tip : Use the following command"
#     echo "export ANDROID_HOME=/path/to/your/android/sdk/directory"
#     echo "******"
#     echo "Tip #2 : You may want to put the export command in your .bach_profile to make the environment variable permanent (~/.bash_profile)"
#     exit 1
# fi

# androidToolsDir="${ANDROID_HOME}/tools"
# if [ ! -d "$androidToolsDir" ]; then
#     echo "Could not locate android tools"
#     echo "Please make sure your ANDROID_HOME environement variable is pointing to the correct directory"
#     echo "We looked at the following location for the tools : ${androidToolsDir}"
#     exit 1
# fi

# androidCommand="${androidToolsDir}/android"

# $androidCommand create lib-project -g -v 1.1.0 \
# --target "android-17" \
# --name $1 \
# --path "${scriptDir}/../${1}" \
# --package $packageNamespace
